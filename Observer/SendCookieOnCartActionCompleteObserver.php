<?php
declare(strict_types=1);

namespace Syte\Tracker\Observer;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Psr\Log\LoggerInterface;
use Syte\Tracker\Model\Config;
use Syte\Tracker\Helper\Data;
use Magento\Checkout\Model\Session;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class SendCookieOnCartActionCompleteObserver implements ObserverInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var CookieManagerInterface
     */
    protected $cookieManager;

    /**
     * @var Json
     */
    protected $jsonHelper;

    /**
     * @var CookieMetadataFactory
     */
    protected $cookieMetadataFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SendCookieOnCartActionCompleteObserver constructor
     *
     * @param Config $config
     * @param Session $checkoutSession
     * @param CookieManagerInterface $cookieManager
     * @param json $jsonHelper
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param RequestInterface $httpRequest
     * @param StoreManagerInterface $storeManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        Config $config,
        Session $checkoutSession,
        CookieManagerInterface $cookieManager,
        Json $jsonHelper,
        CookieMetadataFactory $cookieMetadataFactory,
        RequestInterface $httpRequest,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->checkoutSession = $checkoutSession;
        $this->cookieManager = $cookieManager;
        $this->jsonHelper = $jsonHelper;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->request = $httpRequest;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
    }

    /**
     * Send cookies after cart action
     *
     * @param Observer $observer
     *
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $store = $this->storeManager->getStore();
        if (!$this->config->isServiceActive($store->getId()) || ! $this->config->isEcomEventActive($store->getId())) {
            return $this;
        }

        if ($this->request->isAjax() && !$this->request->getParam('in_cart')) {
            $this->checkoutSession->unsetData(Data::SYTE_TRACKER_SESSION_NAME);
        }

        $productsToAdd = $this->checkoutSession->getData(Data::SYTE_TRACKER_SESSION_NAME);

        if (!empty($productsToAdd) && !$this->request->isXmlHttpRequest()) {
            try {
                $publicCookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
                    ->setDuration(3600)
                    ->setPath('/')
                    ->setHttpOnly(false);

                $this->cookieManager->setPublicCookie(
                    Data::SYTE_TRACKER_COOKIE_NAME,
                    rawurlencode($this->jsonHelper->serialize($productsToAdd)),
                    $publicCookieMetadata
                );
                $this->checkoutSession->unsetData(Data::SYTE_TRACKER_SESSION_NAME);
            } catch (InputException | CookieSizeLimitReachedException | FailureToSendException $e) {
                $errorMessage = sprintf("Can't send cookie quote. Error %s.", $e->getMessage());
                $this->logger->error($errorMessage);
            }
        }

        return $this;
    }
}

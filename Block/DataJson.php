<?php
declare(strict_types=1);

namespace Syte\Tracker\Block;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\SessionException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Item as QuoteItem;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order\Item as OrderItem;
use Magento\Sales\Model\Order;
use Syte\Tracker\Service\GetCurrentProductService;
use Psr\Log\LoggerInterface;

/**
 * Data provider in JSON format.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class DataJson extends Template
{
    /**
     * @var GetCurrentProductService
     */
    private $currentProductService;

    /**
     * @var Json
     */
    private $jsonHelper;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * DataJson constructor
     *
     * @param Context $context
     * @param Json $jsonHelper
     * @param CheckoutSession $checkoutSession
     * @param GetCurrentProductService $currentProductService
     * @param LoggerInterface $logger
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Json $jsonHelper,
        CheckoutSession $checkoutSession,
        GetCurrentProductService $currentProductService,
        LoggerInterface $logger,
        array $data = []
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->currentProductService = $currentProductService;
        $this->checkoutSession = $checkoutSession;
        $this->logger = $logger;

        parent::__construct($context, $data);
    }

    /**
     * Returns current product
     *
     * @return ProductInterface
     */
    public function getCurrentProduct(): ?ProductInterface
    {
        return $this->currentProductService->getProduct();
    }

    /**
     * Get store currency code for page tracking javascript code
     *
     * @return string
     */
    public function getStoreCurrencyCode(): string
    {
        try {
            $store = $this->_storeManager->getStore();
            $currency = $store->getBaseCurrencyCode();
        } catch (NoSuchEntityException $e) {
            $errorMessage = sprintf("Can't get current store. Error %s.", $e->getMessage());
            $this->logger->error($errorMessage);
            $currency = '';
        }

        return $currency;
    }

    /**
     * Generates json array of all products in the cart for javascript on each checkout step
     *
     * @return string
     */
    public function getCartContent(): string
    {
        $cart = [];

        try {
            /** @var Quote $quote */
            $quote = $this->getCheckoutSession()->getQuote();

            foreach ($quote->getAllVisibleItems() as $item) {
                $cart[] = $this->formatProduct($item);
            }
        } catch (LocalizedException $e) {
            $errorMessage = sprintf("Can't get current quote. Error %s.", $e->getMessage());
            $this->logger->error($errorMessage);
        }

        return $this->jsonHelper->serialize($cart);
    }

    /**
     * Calculate current cart total with shipping fee
     *
     * @return float
     */
    public function getCurrentCartTotals(): float
    {
        $total = 0;

        try {
            /** @var Quote $quote */
            $quote = $this->getCheckoutSession()->getQuote();

            foreach ($quote->getAllVisibleItems() as $item) {
                $total += $item->getPrice() * $item->getQty();
            }
            $total += $quote->getShippingAddress()->getShippingAmount();
        } catch (LocalizedException $e) {
            $errorMessage = sprintf("Can't get current quote. Error %s.", $e->getMessage());
            $this->logger->error($errorMessage);
        }

        return $total;
    }

    /**
     * Generates json array of all products in the cart for javascript on each checkout step
     *
     * @return string
     */
    public function getCartContentForUpdate(): string
    {
        $cart = [];

        try {
            /** @var Quote $quote */
            $quote = $this->getCheckoutSession()->getQuote();

            foreach ($quote->getAllVisibleItems() as $item) {
                $cart[$item->getSku()]= $this->formatProduct($item);
            }
        } catch (LocalizedException $e) {
            $errorMessage = sprintf("Can't get current quote. Error %s.", $e->getMessage());
            $this->logger->error($errorMessage);
        }

        return $this->jsonHelper->serialize($cart);
    }

    /**
     * Calculate last order total with shipping fee
     *
     * @return float
     */
    public function getLastOrderCartTotals(): float
    {
        $total = 0;
        $order = $this->checkoutSession->getLastRealOrder();

        /** @var $item OrderItemInterface */
        foreach ($order->getAllVisibleItems() as $item) {
            $total += $item->getPrice() * $item->getQtyOrdered();
        }
        $total += $order->getShippingAmount();

        return $total;
    }

    /**
     * Generates json array of all products in the last orders cart for javascript
     *
     * @return string
     */
    public function getLastOrderCartContent(): string
    {
        $cart = [];
        $order = $this->checkoutSession->getLastRealOrder();

        foreach ($order->getAllVisibleItems() as $item) {
            $cart[] = $this->formatProduct($item);
        }

        return $this->jsonHelper->serialize($cart);
    }

    /**
     * Get last order ID
     */
    public function getCurrentOrderId(): string
    {
        $order = $this->checkoutSession->getLastRealOrder();

        return $order->getIncrementId();
    }

    /**
     * Format product item for output to json
     *
     * @param QuoteItem|OrderItem $item
     * @return array
     */
    private function formatProduct($item): array
    {
        $product = [];
        $product['sku'] = $item->getSku();
        $product['price'] = (float)$item->getPrice();
        $product['qty'] = (int)$item->getQty() ?: (int)$item->getQtyOrdered();

        return $product;
    }

    /**
     * Returns checkout session object.
     *
     * @return CheckoutSession
     * @throws SessionException
     */
    private function getCheckoutSession(): CheckoutSession
    {
        if (!$this->checkoutSession->isSessionExists()) {
            $this->checkoutSession->start();
        }

        return $this->checkoutSession;
    }
}

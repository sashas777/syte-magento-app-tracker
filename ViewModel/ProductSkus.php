<?php
declare(strict_types=1);

namespace Syte\Tracker\ViewModel;

use Syte\Tracker\Service\GetCurrentProductService;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Syte\Tracker\Helper\Product\ChildrenSkus\Data as Helper;

/**
 * Get parent and if needed child product skus
 */
class ProductSkus implements ArgumentInterface
{
    /**
     * @var GetCurrentProductService
     */
    private $currentProductService;

    /**
     * @var ProductInterface|null
     */
    private $product;

    /**
     * @var Helper
     */
    private $helper;

    /**
     * ProductSkus constructor
     *
     * @param GetCurrentProductService $currentProductService
     * @param Helper $helper
     */
    public function __construct(
        GetCurrentProductService $currentProductService,
        Helper $helper
    ) {
        $this->currentProductService = $currentProductService;
        $this->helper = $helper;
    }

    /**
     * Get product children skus html
     *
     * @return string
     */
    public function getSkusHtml(): string
    {
        if (!$this->getProduct()) {
            return '<div class="syte-product-group" masterID="" variants=""></div>';
        }

        $product = $this->getProduct();

        return $this->helper->getChildrenSkusHtml($product);
    }

    /**
     * Get product
     *
     * @return ProductInterface|null
     */
    private function getProduct(): ?ProductInterface
    {
        if (!$this->product) {
            $this->product = $this->currentProductService->getProduct();
        }

        return $this->product;
    }
}

<?php
declare(strict_types=1);

namespace Syte\Tracker\Model;

use Syte\Core\Model\Config as CoreConfig;
use Syte\Core\Model\Constants;

class Config extends CoreConfig
{
    public const SYTE_CONFIG_ACTIVE = 'active';
    public const SYTE_CONFIG_ECOM_EVENT_ACTIVE = 'ecomm_event_active';
    public const SYTE_CONFIG_MODE = 'mode';
    public const SYTE_CONFIG_MODE_INTEGRATION = 'mode_integration';
    public const SYTE_CONFIG_MODE_SEARCH = 'mode_search';
    public const SYTE_CONFIG_MODE_PIXEL_LOAD = 'mode_pixel_load';
    public const SYTE_CONFIG_MODE_PIXEL_INIT = 'mode_pixel_init';

    /**
     * Get config area value by short field name
     *
     * @param string $fieldShort
     * @param int|string|null $storeId
     *
     * @return null|int|string
     */
    private function getAreaConfig(string $fieldShort, $storeId = null)
    {
        return $this->getConfigValue(Constants::SYTE_CONFIG_PATH_SCRIPT . $fieldShort, $storeId);
    }

    /**
     * Get active status
     *
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function isServiceActive($storeId = null): bool
    {
        $isActive = (int)$this->getAreaConfig(self::SYTE_CONFIG_ACTIVE, $storeId);

        return (bool)$isActive;
    }


    /**
     * Get E-commerce Events status
     *
     * @param int|string|null $storeId
     *
     * @return bool
     */
    public function isEcomEventActive($storeId = null): bool
    {
        $isActive = (int)$this->getAreaConfig(self::SYTE_CONFIG_ECOM_EVENT_ACTIVE, $storeId);

        return (bool)$isActive;
    }

    /**
     * Get tracking scripts
     *
     * @param int|string|null $storeId
     *
     * @return array
     */
    public function getScriptContents(?int $storeId = null): array
    {
        $content = [];
        if ($this->isServiceActive($storeId) && $this->isAccountActive($storeId)) {
            $mode = $this->getAreaConfig(self::SYTE_CONFIG_MODE, $storeId);
            switch ($mode) {
                case 'integration':
                    $content['head'] = (string)$this->getAreaConfig(
                        self::SYTE_CONFIG_MODE_INTEGRATION,
                        $storeId
                    );
                    break;
                case 'search':
                    $content['head'] = (string)$this->getAreaConfig(self::SYTE_CONFIG_MODE_SEARCH, $storeId);
                    break;
                case 'pixel':
                    $content['head'] = (string)$this->getAreaConfig(
                        self::SYTE_CONFIG_MODE_PIXEL_LOAD,
                        $storeId
                    );
                    $content['init'] = (string)$this->getAreaConfig(
                        self::SYTE_CONFIG_MODE_PIXEL_INIT,
                        $storeId
                    );
                    break;
            }
        }

        return isset($content['head']) ? $content : [];
    }
}

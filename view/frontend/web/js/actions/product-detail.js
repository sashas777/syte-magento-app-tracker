define([
    'jquery'
], function ($) {
    'use strict';

    /**
     * Dispatch product detail event to Syte Tracker
     *
     * @param {Object} data - product data
     *
     * @private
     */
    function send(data) {
        var syteData = {
            name: 'fe_page_view',
            tag: 'ecommerce',
            sku: data.sku
        };
        window.syteDataLayer.push(syteData);
    }

    return function (productData) {
        send(productData);
    };
});

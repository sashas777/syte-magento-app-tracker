define([
    'jquery',
], function ($) {
    'use strict';

    /**
     * Dispatch checkout start to Syte Tracker
     *
     * @param {Object} cart - cart data
     * @param {float} total - current cart total
     * @param {string} currentCurrency - current currency
     *
     * @private
     */
    function send(cart, total, currentCurrency) {
        var syteData = {
            name: 'checkout_start',
            tag: 'ecommerce',
            value: total,
            currency: currentCurrency,
            products: cart
        };
        window.syteDataLayer.push(syteData);
    }

    return function (data) {
        send(data.cart, data.cartTotal, data.currentCurrency)
    };
});


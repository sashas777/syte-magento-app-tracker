<?php
declare(strict_types=1);

namespace Syte\Tracker\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Syte\Core\Model\Services\Scripts as ScriptService;
use Syte\Tracker\Model\Config;

class Data extends AbstractHelper
{
    public const SYTE_TRACKER_COOKIE_NAME = 'add_to_cart';
    public const SYTE_TRACKER_SESSION_NAME = 'syte_tracker_products_addtocart';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var ScriptService
     */
    private $scriptService;

    /**
     * Data constructor
     *
     * @param Context $context
     * @param Config $config
     * @param ScriptService $scriptService
     */
    public function __construct(
        Context $context,
        Config $config,
        ScriptService $scriptService
    ) {
        $this->config = $config;
        $this->scriptService = $scriptService;

        parent::__construct($context);
    }

    /**
     * Get tracking scripts from config
     *
     * @param int $storeId
     *
     * @return array
     */
    public function getScriptContents(int $storeId): array
    {
        $content = $this->config->getScriptContents($storeId);
        foreach ($content as $area => $contentValue) {
            $this->scriptService->replaceScriptContentAnchors($content[$area], $storeId);
        }

        return $content;
    }
}
